import type { MaybeRef } from '#imports'
import type { JobState, JobTerminalState } from '@rplanel/galaxy-js'
import { JobTerminalStates } from '@rplanel/galaxy-js'

export function useGalaxyJobState() {
  const isTerminalState = (state: MaybeRef<JobState | null> = null) => {
    const stateVal = toValue(state)
    if (stateVal) {
      return JobTerminalStates.includes(toValue(state) as JobTerminalState)
    }
    else { return false }
  }

  return { isTerminalState }
}

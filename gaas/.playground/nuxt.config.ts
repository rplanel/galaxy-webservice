export default defineNuxtConfig({
  extends: ['..'],
  modules: ['@nuxt/eslint'],
  future: {
    compatibilityVersion: 4,
  },
  runtimeConfig: {
    authTokenName: 'sb-eazhbrrdowqbzbbfjgxa-auth-token',
  },
  compatibilityDate: '2024-10-03',

  nitro: {
    experimental: {
      openAPI: true,
    },
  },
})

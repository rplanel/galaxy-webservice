// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  // extends: ['gitlab:rplanel/galaxy-webservice/galaxy'],
  modules: ['@nuxtjs/supabase', '@nuxt/ui-pro', '@nuxt/eslint', 'nuxt-galaxy'],

  imports: {
    autoImport: true,
  },
  devtools: { enabled: true },

  // plugins: ['../app/plugins/galaxy.server.ts'],
  css: ['../app/assets/css/main.css'],

  runtimeConfig: {
    authTokenName: 'sb-127-auth-token',
    // galaxy: {
    //   apiKey: '',
    //   email: '',
    // },
    supabase: {
      authTokenName: 'sb-127-auth-token',
    },

    public: {
      // galaxy: {
      //   url: ''
      // },
    },
  },
  future: {
    compatibilityVersion: 4,
  },

  // galaxy: {

  // },
  // unocss: {
  //   nuxtLayers: true,
  // },
  supabase: {
    redirectOptions: {
      login: '/login',
      callback: '/confirm',
      include: ['/admin(/*)?'],
      exclude: [],
      cookieRedirect: true,
    },
    clientOptions: {
      db: {
        schema: 'galaxy',
      },
    },
    types: './types/supabase.ts',
  },
})

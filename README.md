# Galaxy as a Service

Implement Nuxt layers in order to have galaxy integration for Nuxt project all setup

## Get started



Create a new nuxt app : 

```bash
pnpm dlx nuxi@latest init <project-name>
```

Start development server: 

```bash
cd <project-name>
pnpm dev
```

Extend app with galaxy-webservice layer :

```js
export default defineNuxtConfig({
  extends: ['gitlab:rplanel/galaxy-webservice/gaas'],
  devtools: { enabled: true },
})
```

Start Supabase instance:

```bash
pnpx supabase start --workdir node_modules/.c12/<rplanel-galaxy-repo>
pnpx supabase db reset --workdir node_modules/.c12/<rplanel-galaxy-repo>
```


Add env variables to connect to galaxy and supabase

```
NUXT_PUBLIC_GALAXY_URL="https://galaxy.pasteur.fr"
NUXT_GALAXY_EMAIL="remi.planel@pasteur.fr"
NUXT_GALAXY_API_KEY="key"
SUPABASE_URL="http://127.0.0.1:54321"
SUPABASE_KEY="supabse_key"
DATABASE_URL="postgresql://postgres:postgres@127.0.0.1:54322/postgres"
GIGET_GITLAB_URL="https://gitlab.pasteur.fr"
```

Install dependencies :

```bash
pnpm add -D @nuxtjs/supabase "github:rplanel/galaxy-js#main" drizzle-orm postgres typescript @nuxt/ui@next zod @tailwindcss/typography jwt-decode
```

Update `app.vue` in order to use page:

```js
<template>
  <div>
    <NuxtLayout>
      <NuxtPage />
    </NuxtLayout>
  </div>
</template>

```

In order to test the layers :

```bash
cd app
pnpm i
pnpm dev
```
